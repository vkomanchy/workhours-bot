import { DateTime } from 'luxon';

export interface UserData {
  chatId: number;
  userName: string;
  isNotificationEnabled: boolean;
  notificationTime: string;
  timeZone?: string;
  currentTimerId?: string;
  ratePerHour?: number;
}

export interface Worklog {
  started: number;
  stopped?: number;
  id: string;
  description: string;
  chatId: number;
  tags: string;
  isPaid?: 0 | 1;
}

interface MessageFrom {
  id: number;
  is_bot: boolean;
  first_name: string;
  last_name: string;
  username: string;
  language_code: string;
}

interface MessageChat {
  id: number;
  first_name: string;
  last_name: string;
  username: string;
  type: 'private';
}

interface UserLocation {
  latitude: number;
  longitude: number;
}
export interface BotMessage {
  date: number;
  text: string;

  message_id: number;
  from: MessageFrom;

  chat: MessageChat;

  location?: UserLocation;
}

export enum ReportType {
  DAY,
  YESTERDAY,

  WEEK,
  PREV_WEEK,

  MONTH,
  PREV_MONTH,

  ALL,
}

export interface CustomRange {
  start: number;
  end: number;
}

export interface WorklogsAndDateForRange {
  dateForRange: DateTime;
  worklogs: Worklog[];

  startOfRange?: DateTime;
  endOfRange?: DateTime;
}
