import { promisify } from 'util';

const sqlite3 = require('sqlite3').verbose();

export const db = new sqlite3.Database('users-database');

db.run = promisify(db.run);
db.get = promisify(db.get);
db.all = promisify(db.all);
