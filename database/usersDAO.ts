import { UserData } from 'types';
import { db } from './db';

class UsersDAO {
  dropTable() {
    db.run('DROP TABLE users');
  }

  initTable() {
    db.run('CREATE TABLE IF NOT EXISTS users (chatId int, userName char, isNotificationEnabled bool, currentTimerId char, timeZone string, ratePerHour int)');
  }

  async _showAllUsers() {
    const allUsers = await db.all('SELECT * FROM users');

    console.log('All users', allUsers);

    return allUsers;
  }

  async getUser(chatId: number): Promise<UserData> {
    const user = await db.get(`SELECT * FROM users WHERE chatId=?`, chatId);

    return user;
  }

  async getUsersForNotification(): Promise<{ chatId: number }[]> {
    const usersWithNotificationEnabled = await db.all('SELECT chatId FROM users where isNotificationEnabled=1');

    return usersWithNotificationEnabled;
  }

  async createUser({ chatId, userName }: UserData) {
    await db.run(`INSERT INTO users (chatId, userName, isNotificationEnabled) VALUES (?, ?, ?)`, chatId, userName, true);

    this._showAllUsers();
  }

  updateUser(chatId: number, field: string, value: string | number) {
    db.run(`UPDATE users SET ${field} = ? WHERE chatId = ?`, value, chatId);
  }

  deleteUser(chatId: number) {
    db.run(`DELETE FROM users WHERE chatId=?`, chatId);
  }
}

const usersDAOSingletone = new UsersDAO();

usersDAOSingletone.initTable();

export {
  usersDAOSingletone,
};

