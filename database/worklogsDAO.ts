import { Worklog } from '../types';
import { db } from './db';

class WorklogsDAO {
  dropTable() {
    db.run('DROP TABLE worklogs');
  }

  initTable() {
    db.run(`CREATE TABLE IF NOT EXISTS worklogs (
      started int, stopped int,
      id char, description text,
      chatId string, tags string,
      isPaid bool)
      `);
  }

  async _showAllWorklogs() {
    db.each('SELECT * FROM worklogs', (error: any, worklog: any) => {
      if (error) {
        console.error('Error while getting all worklogs');
      }

      console.log('worklog', worklog);
    });
  }

  async getWorklog(worklogId: string): Promise<Worklog> {
    const worklog = await db.get(`SELECT * FROM worklogs WHERE id = ?`, worklogId);

    return worklog;
  }

  async createWorklog({ started, description, id, chatId, tags, isPaid, stopped }: Worklog) {
    const data = [started, description, id, chatId, tags, isPaid];

    if (stopped) {
      data.push(stopped);

      await db.run(`INSERT INTO worklogs (started, description, id, chatId, tags, isPaid, stopped) VALUES (?, ?, ?, ?, ?, ?, ?)`, ...data);
    } else {
      await db.run(`INSERT INTO worklogs (started, description, id, chatId, tags, isPaid) VALUES (?, ?, ?, ?, ?, ?)`, ...data);
    }
  }

  async editWorklog({ description, id, tags, isPaid }: Worklog) {
    const data = [description, tags, isPaid, id];

    const result = await db.run(`UPDATE worklogs SET description = ?, tags = ?, isPaid = ? WHERE id = ?`, ...data);
  }

  async updateWorklog(id: string, field: string, value: string | number) {
    await db.run(`UPDATE worklogs SET ${field} = ? WHERE id = ?`, value, id);
  }

  async deleteWorklog(chatId: number, id: string) {
    await db.run(`DELETE FROM worklogs WHERE chatId = ? AND id = ?`, chatId, id);
  }

  async findWorklogsInRange(chatId: number, startDate: number, endDate: number): Promise<Worklog[]> {
    return await db.all('SELECT * FROM worklogs WHERE chatId = ? AND started >= ? AND started <= ?', chatId, startDate, endDate);
  }
}

const worklogsDAOSingletone = new WorklogsDAO();

worklogsDAOSingletone.initTable();

export {
  worklogsDAOSingletone,
};

