require('dotenv/config');

const cron = require('node-cron');

import { BotMessage, ReportType } from './types';

import { usersDAOSingletone } from './database/usersDAO';
import { getWorklogsAndDateForRange } from './getWorklogsAndDateForRange';

import { useRegExp } from './appRegExps';

import { addWorklogReportsToBot, getWorklogReport } from './commands/worklogReports';
import { addSettingsToBot } from './commands/botSettings';
import { addWorklogCommandsToBot } from './commands/worklogCommands';

const TelegramBot = require('node-telegram-bot-api');

const bot = new TelegramBot(process.env.TELEGRAM_TOKEN, { polling: true });

const REPOSITORY_LINK = 'https://codeberg.org/vkomanchy/workhours-bot';

bot.onText(useRegExp('start'), async (msg: BotMessage) => {
  const { id: chatId, username: userName } = msg.chat;

  try {
    const isUserExist = await usersDAOSingletone.getUser(chatId);

    if (!isUserExist) {
      usersDAOSingletone.createUser({ chatId, userName, isNotificationEnabled: true, notificationTime: '18:00' });

      bot.sendMessage(msg.chat.id, 'User was created');
    }
  } catch (error) {
    console.error('WORKHOURS-BOT -- Error while start bot for user - ', error);
  }
});

bot.onText(useRegExp('help'), async (msg: BotMessage) => {
  const { id: chatId } = msg.chat;

  bot.sendMessage(chatId, `For getting help please refer to repository - ${REPOSITORY_LINK}`);
});

cron.schedule('0 20 * * 1-5', async () => {
  try {
    const usersWithNotificationEnabled = await usersDAOSingletone.getUsersForNotification();

    usersWithNotificationEnabled.forEach(
      async ({ chatId }) => {
        const reportType = ReportType.DAY;

        const { ratePerHour, timeZone } = await usersDAOSingletone.getUser(chatId);

        const { dateForRange: currentDate, worklogs } = await getWorklogsAndDateForRange(chatId, reportType, timeZone);

        const worklogReport = getWorklogReport(currentDate, worklogs, reportType, ratePerHour, timeZone);

        bot.sendMessage(chatId, worklogReport);
      },
    );
  } catch (error) {
    console.error('WORKHOURS-BOT -- Error while trying to send notification - ', error);
  }
});

addWorklogReportsToBot(bot);
addSettingsToBot(bot);
addWorklogCommandsToBot(bot);
