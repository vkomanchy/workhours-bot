# workhours-bot

Telegram bot for logging time and getting reports about logged time. Created with [node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api)

Bot id in Telegram: *@WorkhoursCountBot*

All interaction with bot could be maked only through Telegram.

## Registration

To start bot just write `/start` command in bot. This will create new user.

## Quick usage of commands

For quick usage of commands now all commands added in bot commands.

![Commands in Telegram bot](./commands-showcase.png "Commands in Telegram bot")

## Bot settings

| Command | Description | Example |
| ---     | ---         | ---     |
| `/set_rate` | Set rate of user in $. | `/set_rate 15` |
| `/set_timezone` | Set timezone. Timezone should be in IANA format.  | `/set_timezone Europe/Moscow` |
| `/get_timezone_by_location` | Return keyboard with location request. Available only for mobile devices. | `/get_timezone_by_location` |


## Worklog commands

| Command | Description | Example |
| ---     | ---         | ---     |
| `/start_timer` | Create worklog with description, tags (#) and $paid param (show is worklog will be calculated as paid) | `/start_timer Worklog description #MyProject $paid` |
| `/stop_timer` | Set stopped time to current second for worklog. If id specified will stop this worklog, otherwise will stop previously started worklog  | `/stop_timer 9245fd3b-dcea-4696-8d89-7dfffc355fd2` |
| `/delete_worklog` | Delete worklog | `/delete_worklog 9245fd3b-dcea-4696-8d89-7dfffc355fd2` |
| `/edit_worklog` | Edit worklog. Works the same as start, but id should be defined by user | `/edit_worklog id=2d57c03d-ed16-4688-bad3-a3c93584e08c $paid New description` |
| `/create_worklog` | Create worklog. Works the same as start, but logged should be defined by user in seconds. | `/create_worklog Worklog description $paid #TM logged=500` |

## Worklog reports

| Command | Description | Example |
| ---     | ---         | ---     |
| `/today_worklogs` | Get worklogs for today | `/today_worklogs` |
| `/yesterday_worklogs` | Get worklogs for yesterday | `/yesterday_worklogs` |
| `/week_worklogs` | Get worklogs for current week | `/week_worklogs` |
| `/prev_week_worklogs` | Get worklogs for previous week | `/prev_week_worklogs` |
| `/month_worklogs` | Get worklogs for current month | `/month_worklogs` |
| `/prev_month_worklogs` | Get worklogs for previous month | `/prev_month_worklogs` |
| `/get_worklogs_by_dates` | Get worklogs for dates from start to end. start and end should be Unix time | `/get_worklogs_by_dates start=1653414914790 end=1653414914790` |