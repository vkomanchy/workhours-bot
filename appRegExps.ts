const appRegExpKeys = [
  'start', 'startTimer', 'stopTimer', 'todayWorklogs',
  'yesterdayWorklogs', 'weekWorklogs', 'monthWorklogs',
  'prevMonthWorklogs', 'deleteWorklog', 'setRate', 'createWorklog',
  'getTimezoneByLocation', 'setTimezone', 'getWorklogsByDates',
  'editWorklog', 'isPaid', 'tags', 'prevWeekWorklogs',
  'logged', 'start', 'end', 'id', 'help',
] as const;

type AppRegExpsKeys = typeof appRegExpKeys[number];

const RegExps: Map<AppRegExpsKeys, RegExp> = new Map([
  ['start', /\/start/],
  ['startTimer', /\/start_timer/],
  ['stopTimer', /\/stop_timer/],
  ['todayWorklogs', /\/today_worklogs/],
  ['yesterdayWorklogs', /\/yesterday_worklogs/],
  ['weekWorklogs', /\/week_worklogs/],
  ['prevWeekWorklogs', /\/prev_week_worklogs/],
  ['monthWorklogs', /\/month_worklogs/],
  ['prevMonthWorklogs', /\/prev_month_worklogs/],
  ['deleteWorklog', /\/delete_worklog/],
  ['setRate', /\/set_rate/],
  ['createWorklog', /\/create_worklog/],
  ['getTimezoneByLocation', /\/get_timezone_by_location/],
  ['setTimezone', /\/set_timezone/],
  ['getWorklogsByDates', /\/get_worklogs_by_dates/],
  ['editWorklog', /\/edit_worklog/],

  ['isPaid', /\$paid/],
  ['tags', /(?:#)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)/ig],
  ['logged', /logged=(\d*)/ig],

  ['start', /start=\d*/ig],
  ['end', /end=\d*/ig],

  ['id', /id=[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/],
  ['help', /\/help/],
]);

export const useRegExp = (key: AppRegExpsKeys): RegExp => RegExps.get(key)!;
