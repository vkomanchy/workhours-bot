const cron = require('node-cron');

import { BotMessage, ReportType, CustomRange, Worklog } from '../types';
import { usersDAOSingletone } from '../database/usersDAO';
import { getWorklogsAndDateForRange } from '../getWorklogsAndDateForRange';
import { useRegExp } from '../appRegExps';

import { DateTime } from 'luxon';

const humanizeDuration = require('humanize-duration');

const SECONDS_IN_HOUR = 3600;
export const MILLISECONDS_IN_SECOND = 1000;

export const getWorklogReport = (
  currentDate: DateTime,
  worklogs: Worklog[],
  range: ReportType | CustomRange,
  ratePerHour: number = 0,
  timeZone: string = 'Europe/Moscow',
): string[] => {
  const rangeAsCustomRange = range as CustomRange;
  const isCustomRange = rangeAsCustomRange.start && rangeAsCustomRange.end;

  const reportHeader = (() => {
    if (isCustomRange) {
      const formattedStart = DateTime.fromSeconds(rangeAsCustomRange.start, { zone: timeZone }).toFormat('dd LLL, HH:mm:ss');
      const formattedEnd = DateTime.fromSeconds(rangeAsCustomRange.end, { zone: timeZone }).toFormat('dd LLL, HH:mm:ss');

      return `Start: ${formattedStart}, End: ${formattedEnd}`;
    }

    switch (range) {
      case ReportType.DAY:
        return 'Today';

      case ReportType.YESTERDAY:
        return 'Yesterday';

      case ReportType.PREV_WEEK:
      case ReportType.WEEK:
        const startOfWeekFormatted = currentDate.startOf('week').toFormat('dd.LL');
        const endOfWeekFormatted = currentDate.endOf('week').toFormat('dd.LL');

        return `This week (${startOfWeekFormatted} - ${endOfWeekFormatted})`;

      case ReportType.PREV_MONTH:
      case ReportType.MONTH:
        const currentMonth = currentDate.startOf('month').toFormat('LLLL');

        return `This month - ${currentMonth}`;
      default:
        return 'Wrong date';
    }
  })();

  const ratePerSecond = ratePerHour / SECONDS_IN_HOUR;

  const dateTimeNow = DateTime.fromMillis(new Date().valueOf(), { zone: timeZone });

  const totalWorklogsSeconds = worklogs.reduce(
    (acc, current) => {
      const loggedTime = isCustomRange
        ? dateTimeNow.toSeconds() - current.started
        : (current.stopped || currentDate.toSeconds()) - current.started;

      acc += loggedTime;

      return acc;
    },
    0,
  );

  const paidWorklogsTotalInSeconds = worklogs.reduce(
    (acc, current) => {
      const loggedTime = isCustomRange
        ? dateTimeNow.toSeconds() - current.started
        : (current.stopped || currentDate.toSeconds()) - current.started;

      if (current.isPaid) {
        acc += loggedTime;
      }


      return acc;
    },
    0,
  );

  const totalInMillis = totalWorklogsSeconds * MILLISECONDS_IN_SECOND;
  const humanReadableTotal = humanizeDuration(totalInMillis, { round: true });

  const delimiter = '\n-------------------';

  const totalWorklogs = `\nTotal worklogs time: ${humanReadableTotal}`;

  const totalMoney = `\nTotal money: ${Math.round(paidWorklogsTotalInSeconds * ratePerSecond * 100) / 100}$`;

  const worklogHeader = '\nWorklogs:';

  const worklogsAsString = worklogs.reduce(
    (acc, current, index) => {
      const stoppedTime = (isCustomRange && !current.stopped)
        ? dateTimeNow.toSeconds()
        : (current.stopped || currentDate.toSeconds());

      const loggedTime = stoppedTime - current.started;

      acc += `\n${index + 1} ---`;
      acc += `\nStart - ${DateTime.fromSeconds(current.started, { zone: timeZone }).toFormat('dd LLL, yyyy HH:mm:ss')}`;
      acc += `\nEnd - ${DateTime.fromSeconds(stoppedTime, { zone: timeZone }).toFormat('dd LLL, HH:mm:ss')}`;

      acc += `\n${current.description} ${JSON.parse(current.tags).join(' ')}`;
      acc += `\nLogged time -  ${humanizeDuration(loggedTime * MILLISECONDS_IN_SECOND, { round: true })}`;
      acc += `\nWorklog id - ${current.id}`;

      if (current.isPaid) {
        acc += `\nMoney - ${Math.round(loggedTime * ratePerSecond * 100) / 100}$`;
      }

      acc += `\n`;

      return acc;
    },
    '\n',
  );

  const resultReport = reportHeader + delimiter + totalWorklogs + totalMoney + worklogHeader + worklogsAsString;

  const MAX_MESSAGE_LENGTH = 3500;

  const messagesCount = Math.ceil(resultReport.length / MAX_MESSAGE_LENGTH);

  const messages: string[] = [];

  console.log('messagesCount', messagesCount);

  for (let i = 0; i < messagesCount; i += 1) {
    messages.push(resultReport.slice(i * MAX_MESSAGE_LENGTH, (i + 1) * MAX_MESSAGE_LENGTH));
  }

  console.log('messages.length', messages.length);

  return messages;
};

export const addWorklogReportsToBot = (bot: any) => {
  const getWorklogWithReportType = (reportType: ReportType) => async (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const { ratePerHour, timeZone } = await usersDAOSingletone.getUser(chatId);

      const { dateForRange: currentDate, worklogs } = await getWorklogsAndDateForRange(chatId, reportType, timeZone);

      const worklogReport = getWorklogReport(currentDate, worklogs, reportType, ratePerHour, timeZone);

      console.log('worklogReport.length', worklogReport.length);

      for (const worklogReportString of worklogReport) {
        await bot.sendMessage(chatId, worklogReportString);
      }
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while getting report for report type - ', reportType, error);
    }
  };

  bot.onText(useRegExp('todayWorklogs'), getWorklogWithReportType(ReportType.DAY));

  bot.onText(useRegExp('yesterdayWorklogs'), getWorklogWithReportType(ReportType.YESTERDAY));

  bot.onText(useRegExp('weekWorklogs'), getWorklogWithReportType(ReportType.WEEK));

  bot.onText(useRegExp('prevWeekWorklogs'), getWorklogWithReportType(ReportType.PREV_WEEK));

  bot.onText(useRegExp('monthWorklogs'), getWorklogWithReportType(ReportType.MONTH));

  bot.onText(useRegExp('prevMonthWorklogs'), getWorklogWithReportType(ReportType.PREV_MONTH));


  const getWorklogForCustomRange = async (range: CustomRange, chatId: number) => {
    const { ratePerHour, timeZone } = await usersDAOSingletone.getUser(chatId);

    const { dateForRange: currentDate, worklogs } = await getWorklogsAndDateForRange(chatId, range, timeZone);

    const worklogReport = getWorklogReport(currentDate, worklogs, range, ratePerHour, timeZone);

    worklogReport.forEach((worklogReportString) => {
      bot.sendMessage(chatId, worklogReportString);
    });
  };

  bot.onText(useRegExp('getWorklogsByDates'), (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const startRegex = useRegExp('start');
      const endRegex = useRegExp('end');

      const start = msg.text.match(startRegex) ? msg.text.match(startRegex) : [];
      const end = msg.text.match(endRegex) ? msg.text.match(endRegex) : [];

      if (start && end && start[0] && end[0]) {
        const startAsNumber = Number(start[0].replace('start=', '').trim());
        const endAsNumber = Number(end[0].replace('end=', '').trim());

        getWorklogForCustomRange({ start: startAsNumber, end: endAsNumber }, chatId);
      } else {
        bot.sendMessage(chatId, 'Wrong start or end date. Please use next format /get_worklogs_by_dates start=5000 end=5000');
      }
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while get worklogs by dates - ', error);
    }
  });
};
