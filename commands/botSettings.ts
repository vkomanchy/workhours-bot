require('dotenv/config');

import { BotMessage } from '../types';

import { usersDAOSingletone } from '../database/usersDAO';

import { isValidTimeZone } from '../isValidTimezone';
import { useRegExp } from '../appRegExps';

const { find } = require('geo-tz');

export const addSettingsToBot = (bot: any) => {
  const changeUserTimeZone = async (chatId: number, timeZone: string): Promise<void> => {
    await usersDAOSingletone.updateUser(chatId, 'timeZone', timeZone);

    bot.sendMessage(chatId, 'User\'s timezone was updated');
  };

  bot.onText(useRegExp('getTimezoneByLocation'), async (msg: BotMessage) => {
    const opts = {
      reply_markup: JSON.stringify({
        keyboard: [
          [{ text: 'Location Request', request_location: true }],
        ],
        resize_keyboard: true,
        one_time_keyboard: true,
      }),
    };

    bot.sendMessage(msg.chat.id, 'Send your location to get your timezone', opts);
  });

  bot.onText(useRegExp('setTimezone'), async (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const setTimeZoneRegex = useRegExp('setTimezone');

      const timeZone = msg.text.replace(setTimeZoneRegex, '').trim();

      if (!timeZone || timeZone.length < 0 || !isValidTimeZone(timeZone)) {
        bot.sendMessage(chatId, 'Wrong timezone');
        return;
      }

      await changeUserTimeZone(chatId, timeZone);
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while setting timezone - ', error);
    }
  });

  bot.on('location', async (msg: BotMessage) => {
    if (msg.location) {
      try {
        const chatId = msg.chat.id;
        const { longitude, latitude } = msg.location;

        const timeZone: string[] = find(latitude, longitude);

        await changeUserTimeZone(chatId, timeZone[0]);
      } catch (error) {
        console.error('WORKHOURS-BOT -- Error while set timezone from user location - ', error);
      }
    }
  });

  bot.onText(useRegExp('setRate'), async (msg: BotMessage) => {
    const chatId = msg.chat.id;

    const ratePerHour = msg.text
      .replace(useRegExp('setRate'), '')
      .trim();

    if (ratePerHour && ratePerHour.length > 0 && !isNaN(Number(ratePerHour))) {
      try {
        await usersDAOSingletone.updateUser(chatId, 'ratePerHour', Number(ratePerHour));

        bot.sendMessage(chatId, `Your rate was set to ${ratePerHour}$`);
      } catch (error) {
        console.error('WORKHOURS-BOT -- Error while trying to set rate - ', error);
      }
    }
  });
};
