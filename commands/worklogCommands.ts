require('dotenv/config');

import { worklogsDAOSingletone } from '../database/worklogsDAO';
import { BotMessage, Worklog } from '../types';
import { v4 as uuidv4 } from 'uuid';

import { usersDAOSingletone } from '../database/usersDAO';

import humanizeDuration = require('humanize-duration');
import { DateTime } from 'luxon';

import { useRegExp } from '../appRegExps';

import { MILLISECONDS_IN_SECOND } from './worklogReports';

export const addWorklogCommandsToBot = (bot: any) => {
  bot.onText(useRegExp('startTimer'), async (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const tagsRegex = useRegExp('tags');

      const description = msg.text
        .replace(useRegExp('startTimer'), '')
        .replace(tagsRegex, '')
        .replace(useRegExp('isPaid'), '')
        .trim();

      const foundTags = msg.text.match(tagsRegex) ? msg.text.match(tagsRegex) : [];

      const tags = JSON.stringify(foundTags);
      const isPaid = useRegExp('isPaid').test(msg.text);
      const id = uuidv4();

      await worklogsDAOSingletone.createWorklog({
        started: msg.date,
        description,
        tags,
        chatId,
        isPaid: isPaid ? 1 : 0,
        id,
      });

      const isUserExist = await usersDAOSingletone.getUser(chatId);

      if (!isUserExist) {
        await usersDAOSingletone.createUser({
          chatId,
          userName: msg.chat.username,
          isNotificationEnabled: true,
          notificationTime: '18:00',
        });

        bot.sendMessage(chatId, 'User was created');
      }

      await usersDAOSingletone.updateUser(chatId, 'currentTimerId', id);

      bot.sendMessage(chatId, `Worklog with description - ${description} was started`);
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while start timer', error);
    }
  });

  const getReportAboutStoppedWorklog = async ({ stopped, started, description }: Worklog, chatId: number) => {
    const loggedTimeForWorklog = stopped! - started;

    const humanReadableLoggedTime = humanizeDuration(loggedTimeForWorklog * MILLISECONDS_IN_SECOND, { round: true });

    bot.sendMessage(chatId, `Worklog with description - ${description} was stopped. Worklog time was - ${humanReadableLoggedTime}`);
  };

  bot.onText(useRegExp('stopTimer'), async (msg: BotMessage) => {
    const chatId = msg.chat.id;
    const idToStop = msg.text.replace(useRegExp('stopTimer'), '').trim();

    try {
      const currentUser = await usersDAOSingletone.getUser(msg.chat.id);

      if (idToStop.length > 0) {
        const foundWorklog = await worklogsDAOSingletone.getWorklog(idToStop);

        if (foundWorklog) {
          await worklogsDAOSingletone.updateWorklog(foundWorklog.id, 'stopped', msg.date);

          if (currentUser.currentTimerId === idToStop) {
            await usersDAOSingletone.updateUser(chatId, 'currentTimerId', '');

            const worklog = await worklogsDAOSingletone.getWorklog(idToStop);

            getReportAboutStoppedWorklog(worklog, chatId);
          }
        } else {
          bot.sendMessage(chatId, 'Started worklog not found. Please start worklog and then stop it');
        }
      } else if (currentUser && currentUser.currentTimerId && currentUser.currentTimerId.length > 0) {
        await worklogsDAOSingletone.updateWorklog(currentUser.currentTimerId, 'stopped', msg.date);

        const worklog = await worklogsDAOSingletone.getWorklog(currentUser.currentTimerId);

        await usersDAOSingletone.updateUser(chatId, 'currentTimerId', '');

        getReportAboutStoppedWorklog(worklog, chatId);
      } else {
        bot.sendMessage(chatId, 'Started worklog not found. Please start worklog and then stop it');
      }
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while trying to stop timer of worklog - ', error);
    }
  });

  bot.onText(useRegExp('deleteWorklog'), async (msg: BotMessage) => {
    const chatId = msg.chat.id;

    try {
      const worklogIdToDelete = msg.text
        .replace(useRegExp('deleteWorklog'), '')
        .trim();

      await worklogsDAOSingletone.deleteWorklog(chatId, worklogIdToDelete);

      bot.sendMessage(chatId, `Worklog with id - ${worklogIdToDelete} was deleted`);
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while trying to delete worklog - ', error);
    }
  });

  bot.onText(useRegExp('createWorklog'), async (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const tagsRegex = useRegExp('tags');

      const isPaidRegex = useRegExp('isPaid');
      const loggedTimeRegex = useRegExp('logged');

      const loggedTime = msg.text.match(loggedTimeRegex);

      if (!loggedTime) {
        bot.sendMessage(chatId, 'Logged time is empty or format is not valid. Please use number of seconds as value');
        bot.sendMessage(chatId, 'Example - logged=500');

        return;
      }

      const description = msg.text
        .replace(useRegExp('createWorklog'), '')
        .replace(isPaidRegex, '')
        .replace(tagsRegex, '')
        .replace(loggedTimeRegex, '')
        .trim();

      const { timeZone = 'Europe/Moscow' } = await usersDAOSingletone.getUser(chatId);

      const foundTags = msg.text.match(tagsRegex) ? msg.text.match(tagsRegex) : [];

      const loggedTimeValue = Number(loggedTime[0].replace('logged=', ''));

      const stopped = DateTime
        .fromSeconds(msg.date, { zone: timeZone })
        .plus({ seconds: loggedTimeValue })
        .toSeconds();

      const tags = JSON.stringify(foundTags);
      const isPaid = isPaidRegex.test(msg.text);
      const id = uuidv4();

      await worklogsDAOSingletone.createWorklog({
        started: msg.date,
        stopped,
        description,
        tags,
        chatId,
        isPaid: isPaid ? 1 : 0,
        id,
      });

      const humanReadableLoggedTime = humanizeDuration(loggedTimeValue * MILLISECONDS_IN_SECOND, { round: true });

      bot.sendMessage(chatId, `Worklog with description - ${description} was created. Worklog logged time - ${humanReadableLoggedTime}`);
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while creating worklog - ', error);
    }
  });

  bot.onText(useRegExp('editWorklog'), async (msg: BotMessage) => {
    try {
      const chatId = msg.chat.id;

      const tagsRegex = useRegExp('tags');

      const idRegex = useRegExp('id');
      const isPaidRegex = useRegExp('isPaid');

      const description = msg.text
        .replace(useRegExp('editWorklog'), '')
        .replace(tagsRegex, '')
        .replace(idRegex, '')
        .replace(isPaidRegex, '')
        .trim();

      const foundTags = msg.text.match(tagsRegex) ? msg.text.match(tagsRegex) : [];
      const idMatch = msg.text.match(idRegex) ? msg.text.match(idRegex) : [];

      if (!idMatch || idMatch.length <= 0) {
        bot.sendMessage(chatId, 'Id of worklog wrong or empty');
      } else {
        const id = idMatch[0].replace('id=', '');
        const tags = JSON.stringify(foundTags);
        const isPaid = isPaidRegex.test(msg.text);

        await worklogsDAOSingletone.editWorklog({
          started: 0,
          description,
          tags,
          chatId,
          isPaid: isPaid ? 1 : 0,
          id,
        });

        const isUserExist = await usersDAOSingletone.getUser(chatId);

        if (!isUserExist) {
          await usersDAOSingletone.createUser({
            chatId,
            userName: msg.chat.username,
            isNotificationEnabled: true,
            notificationTime: '18:00',
          });

          bot.sendMessage(chatId, 'User was created');
        }

        bot.sendMessage(chatId, `Worklog with description - ${description} was edited`);
      }
    } catch (error) {
      console.error('WORKHOURS-BOT -- Error while edit worklog - ', error);
    }
  });
};
