import { worklogsDAOSingletone } from './database/worklogsDAO';
import { DateTime, DateTimeUnit } from 'luxon';
import { CustomRange, ReportType, Worklog, WorklogsAndDateForRange } from './types';
import { MILLISECONDS_IN_SECOND } from './commands/worklogReports';

const mapReportTypeToRange = new Map([
  [ReportType.DAY, 'day'],
  [ReportType.YESTERDAY, 'day'],
  [ReportType.MONTH, 'month'],
  [ReportType.PREV_MONTH, 'month'],
  [ReportType.WEEK, 'week'],
  [ReportType.PREV_WEEK, 'week'],
]);

const getWorklogsForRange = async (start: DateTime, end: DateTime, chatId: number): Promise<Worklog[]> => {
  const startOfRangeInSeconds = start.toSeconds();
  const endOfRangeInSeconds = end.toSeconds();

  return await worklogsDAOSingletone.findWorklogsInRange(chatId, startOfRangeInSeconds, endOfRangeInSeconds);
};

export const getWorklogsAndDateForRange = async (
  chatId: number,
  range: CustomRange | ReportType,
  timeZone: string = 'Europe/Moscow',
): Promise<WorklogsAndDateForRange> => {
  const rangeAsCustomRange = range as CustomRange;

  if (rangeAsCustomRange.start && rangeAsCustomRange.end) {
    const startDate = DateTime.fromMillis(rangeAsCustomRange.start * MILLISECONDS_IN_SECOND, { zone: timeZone });
    const endDate = DateTime.fromMillis(rangeAsCustomRange.end * MILLISECONDS_IN_SECOND, { zone: timeZone });

    const worklogs = await getWorklogsForRange(startDate, endDate, chatId);

    return {
      dateForRange: startDate,

      startOfRange: startDate,
      endOfRange: endDate,

      worklogs,
    };
  }

  const rangeAsReportType = range as ReportType;

  const currentDate = (() => {
    const todayDateTime = DateTime.fromMillis(new Date().valueOf(), { zone: timeZone });

    if (rangeAsReportType === ReportType.PREV_MONTH) {
      const startOfMonth = todayDateTime.startOf('month');

      const prevMonthDateTime = startOfMonth.minus({ hours: 48 });

      return prevMonthDateTime;
    }

    if (rangeAsReportType === ReportType.PREV_WEEK) {
      const startOfWeek = todayDateTime.startOf('week');

      const prevWeekDateTime = startOfWeek.minus({ hours: 48 });

      return prevWeekDateTime;
    }

    if (rangeAsReportType === ReportType.YESTERDAY) {
      const yesterdayDateTime = todayDateTime.minus({ hours: 24 });

      return yesterdayDateTime;
    }

    return todayDateTime;
  })();

  const startOfRange = currentDate.startOf(mapReportTypeToRange.get(range as ReportType) as DateTimeUnit);
  const endOfRange = currentDate.endOf(mapReportTypeToRange.get(range as ReportType) as DateTimeUnit);

  const worklogs = await getWorklogsForRange(startOfRange, endOfRange, chatId);

  return {
    dateForRange: currentDate,
    worklogs,
  };
};
